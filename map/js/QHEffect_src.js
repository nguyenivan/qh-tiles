QHE = function() {
	
    this.slideMsg = function(b) {
        QHM.topMsg.style.bottom = b+"px";
        if (b<0) {
            setTimeout("QHE.slideMsg("+(b+5)+")", 50);
        }
    };
    
    this.toggleR = function() {
		if (typeof this.isO == "undefined") {
			this.isO = true;
		}
		this.aR = $g("QHd");
		if (this.isO) {
			this.oR2L(260);
			this.isO = false;
		} else {
			this.oL2R(260);
			this.isO = true;
		}
	};
	
	this.oR2L = function(b) {
		this.aR.style.right = (b-260)+"px";
		if (b>0) {
			setTimeout("QHE.oR2L("+(b-20)+")", 20);
		}
	};
    
	this.oL2R = function(b) {
		this.aR.style.right = (0-b)+"px";
		if (b>0) {
			setTimeout("QHE.oL2R("+(b-20)+")", 20);
		}
	};
    
    this.setupFS = function(stepTop, stepLeft, stepBottom, stepRight, stepOpacity) {
		this.pTop = stepTop;
		this.pLeft = stepLeft;
		this.pBottom = stepBottom;
		this.pRight = stepRight;
		this.pOpa = stepOpacity;
	};
	
	this.popupFS = function(top, left, bottom, right, maxH, maxW, opacity) {
		var height = maxH - top - bottom, width = maxW - left - right;
		if (top<=0 || left<=0) {
			QHM.ovlSld.style.display = 'block';
			QHM.ovlSld.parentNode.removeChild(this.aniD);
			QHM.aniD = null;
		} else {
			QHM.aniD.style.top = top + 'px';
			QHM.aniD.style.left = left + 'px';
			QHM.aniD.style.width = width + 'px';
			QHM.aniD.style.height = height + 'px';
			QHM.aniD.style.opacity = opacity/10;
			QHM.aniD.style.filter = 'alpha(opacity='+(opacity*10)+')';
			this.pTop = this.pTop*1.5;
			this.pLeft = this.pLeft*1.5;
			this.pBottom = this.pBottom*1.5;
			this.pRight = this.pRight*1.5;
			setTimeout('QHE.popupFS('+(top-this.pTop)+','+(left-this.pLeft)+','+(bottom-this.pBottom)+','+(right-this.pRight)+','+maxH+','+maxW+','+(opacity+this.pOpa)+')', 30);
		}
	};
	
	
	return this;
}();

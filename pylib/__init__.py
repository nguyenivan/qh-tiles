﻿
import re
import models

INTAB = "ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđ"
INTAB = [ch.encode('utf8') for ch in unicode(INTAB, 'utf8')]

OUTTAB = "a"*17 + "o"*17 + "e"*11 + "u"*11 + "i"*5 + "y"*5 + "d"

retab = re.compile("|".join(INTAB))
replaces_dict = dict(zip(INTAB, OUTTAB))

def khongdau(utf8_str):
    return retab.sub(lambda m: replaces_dict[m.group(0)], utf8_str.encode('utf8'))

def k(key, val):
	return '"'+key+'":"'+val+'"'

def parse_project(item):
	out = "{"
	out += k("id",str(item.key().id())) + ","
	out += k("name",item.name) + ","
	out += k("description",item.description) + ","
	out += k("type",item.type) + ","
	out += k("district",item.district) + ","
	out += k("city",item.city) + ","
	out += k("lat",item.lat) + ","
	out += k("lon",item.lon) + ","
	out += k("address",item.address) + ","
	out += k("link",item.link) + ","
	out += k("zoom",item.zoom) + ","
	out += k("investor",item.investor)
	out += "}"
	return out

def json_projects(projects):
	out = "["
	n = len(projects)
	i = 0
	for item in projects:
		out += parse_project(item)
		i=i+1
		if i<n:
			out += ', '
	out += "]"
	return out

def parse_news(item):
	out = "{"
	out += k("id",str(item.key().id())) + ","
	out += k("title",item.title) + ","
	out += k("teaser",item.teaser) + ","
	out += k("source",item.source) + ","
	out += k("link",item.link) + ","
	out += '"project":' + parse_project(item.projectKey)
	out += "}"
	return out

def json_news(news):
	out = "["
	n = len(news)
	i = 0
	for item in news:
		out += parse_news(item)
		i=i+1
		if i<n:
			out += ', '
	out += "]"
	return out

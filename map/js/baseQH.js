$l1 = "Đường Sá";
$l2 = "Vệ Tinh";
$l3 = "Những dự án trong khu vực này";
$l4 = "Sơ đồ bố trí căn hộ";
$l5 = "Tầng";
$l6 = "lửng";

var checkmouseposition = false;
var mousePosX = '';
var mousePosY = '';
var outputCoord = '';
var overlaySlideshow = null;
var animateDiv = null;

var sketchSymbolizers = {
	"Point": {
		pointRadius: 4,
		graphicName: "circle",
		fillColor: "red",
		fillOpacity: 1,
		strokeWidth: 1,
		strokeOpacity: 1,
		strokeColor: "#333333"
	},
	"Line": {
		strokeWidth: 3,
		strokeOpacity: 1,
		strokeColor: "#DE9619",
		strokeDashstyle: "solid"
	},
	"Polygon": {
		strokeWidth: 2,
		strokeOpacity: 1,
		strokeColor: "#DE9619",
		fillColor: "white",
		fillOpacity: 0.3
	}
};


OpenLayers.Util.getImagesLocation = function(){
	return 'img/';
};

OpenLayers.Control.PanZoomBar.prototype.draw = function(px) {
	OpenLayers.Control.prototype.draw.apply(this, arguments);
	px = this.position.clone();
	
	this.buttons = [];

	var sz = new OpenLayers.Size(51,46);
	var centered = new OpenLayers.Pixel(0, 0);

	px.y = centered.y+5;
	this._addButton("zoomworld", "zoom-world-mini.png", px.add(1, 0), sz);
	this._addButton("zoomin", "zoom-plus-mini.png", px.add(1, sz.h+4), sz);
	centered = this._addZoomBar(px.add(1, sz.h*2+4));
	this.slider.style.width = '21px';
	this.slider.style.height = '7px';
	this.slider.childNodes[0].style.width = '21px';
	this.slider.childNodes[0].style.height = '7px';
	this._addButton("zoomout", "zoom-minus-mini.png", centered, sz);
	return this.div;
};


OpenLayers.Control.LayerSwitcher.prototype.loadContents = function() {

	//-----------declare and add button-----------
    theHTML = '';
    theHTML += '<a id="control-none" onclick="toggleControl(this, \'none\');" class="active">Di chuyển</a>';
    theHTML += '<a id="control-line" onclick="toggleControl(this, \'line\');">Khoảng cách</a>';
    theHTML += '<a id="control-polygon" onclick="toggleControl(this, \'polygon\');">Diện tích</a>';
    
    var DistanceDiv = document.createElement("div");
    DistanceDiv.id = 'DistanceDiv';
    DistanceDiv.innerHTML = theHTML;
    this.div.appendChild(DistanceDiv);
	//-----------end of declare and add button--------

    //configure main div

		removeClickEvent(this.div);
		
    // layers list div        
    this.layersDiv = document.createElement("div");
    this.layersDiv.id = this.id + "_layersDiv";
    OpenLayers.Element.addClass(this.layersDiv, "layersDiv");

    this.baseLbl = document.createElement("div");
    this.baseLbl.innerHTML = OpenLayers.i18n("baseLayer");
    OpenLayers.Element.addClass(this.baseLbl, "baseLbl");
    
    this.baseLayersDiv = document.createElement("div");
    OpenLayers.Element.addClass(this.baseLayersDiv, "baseLayersDiv");

    this.dataLbl = document.createElement("div");
    this.dataLbl.innerHTML = OpenLayers.i18n("overlays");
    OpenLayers.Element.addClass(this.dataLbl, "dataLbl");
    
    this.dataLayersDiv = document.createElement("div");
    OpenLayers.Element.addClass(this.dataLayersDiv, "dataLayersDiv");

    if (this.ascending) {
        this.layersDiv.appendChild(this.baseLbl);
        this.layersDiv.appendChild(this.baseLayersDiv);
        this.layersDiv.appendChild(this.dataLbl);
        this.layersDiv.appendChild(this.dataLayersDiv);
    } else {
        this.layersDiv.appendChild(this.dataLbl);
        this.layersDiv.appendChild(this.dataLayersDiv);
        this.layersDiv.appendChild(this.baseLbl);
        this.layersDiv.appendChild(this.baseLayersDiv);
    }    

    this.div.appendChild(this.layersDiv);

    var imgLocation = OpenLayers.Util.getImagesLocation();
    var sz = new OpenLayers.Size(0,0);        

		
    // maximize button div
    var img = imgLocation + 'layer-switcher-maximize.png';
    this.maximizeDiv = OpenLayers.Util.createAlphaImageDiv(
                                "OpenLayers_Control_MaximizeDiv", 
                                null, 
                                sz, 
                                img, 
                                "absolute");
    OpenLayers.Element.addClass(this.maximizeDiv, "maximizeDiv");
    this.maximizeDiv.style.display = "none";
    OpenLayers.Event.observe(this.maximizeDiv, "click", 
        OpenLayers.Function.bindAsEventListener(this.maximizeControl, this)
    );
    
    this.div.appendChild(this.maximizeDiv);

    // minimize button div
    var img = imgLocation + 'layer-switcher-minimize.png';
    var sz = new OpenLayers.Size(0,0);        
    this.minimizeDiv = OpenLayers.Util.createAlphaImageDiv(
                                "OpenLayers_Control_MinimizeDiv", 
                                null, 
                                sz, 
                                img, 
                                "absolute");
    OpenLayers.Element.addClass(this.minimizeDiv, "minimizeDiv");
    this.minimizeDiv.style.display = "none";
    OpenLayers.Event.observe(this.minimizeDiv, "click", 
        OpenLayers.Function.bindAsEventListener(this.minimizeControl, this)
    );

    this.div.appendChild(this.minimizeDiv);

}

if (document.all) {
	document.onmousemove=captureMousePosition;
} else {
	window.onmousemove=captureMousePosition;
}

window.onkeyup = function(e) {
	if(window.event) {
		keynum = e.keyCode
	} else if(e.which) {
		keynum = e.which
	}
	if (keynum == 27) {
		if (overlaySlideshow!='') {
			removeSlideshow();
		}
		var btn = document.getElementById('control-none');
		toggleControl(btn, 'none');
	}
}

function captureMousePosition(event) {
	if (document.all) {
		mousePosX = window.event.clientX;
		mousePosY = window.event.clientY;
	} else {
		mousePosX = event.pageX;
		mousePosY = event.pageY;
	}
	if (checkmouseposition) {
		if (outputCoord == '') {
			outputCoord = document.createElement('div');
			outputCoord.id = 'outputCoord';
			document.getElementById('map').parentNode.appendChild(outputCoord);
			outputCoord.style.display = 'none';
		}
		l = mousePosX + 10;
		t = mousePosY + 20;
		outputCoord.style.left = l + 'px';
		outputCoord.style.top = t + 'px';
	}
}

function handleMeasurements(event) {
    var geometry = event.geometry;
    var units = event.units;
    var order = event.order;
    var measure = event.measure;
    var element = document.getElementById('outputCoord');
    var out = "";
    if(order == 1) {
        out += "Tổng độ dài: " + measure.toFixed(3) + " " + units;
    } else {
        out += "Tổng diện tích: " + measure.toFixed(3) + " " + units + "<sup>2</" + "sup>";
    }
    element.innerHTML = out;
		outputCoord.style.display = 'block';
}

function toggleControl(element, name) {
	document.getElementById('control-none').className = '';
	checkmouseposition = false;
	if (typeof outputCoord != 'undefined' && outputCoord != '') {
		document.getElementById('map').parentNode.removeChild(outputCoord);
		outputCoord = '';
	}
    for(key in measureControls) {
    	document.getElementById('control-'+key).className = '';
        var control = measureControls[key];
        if(name == key ) {
            control.activate();
            checkmouseposition = true;
        } else {
            control.deactivate();
        }
    }
	element.className = 'active';
}
        
function toggleGeodesic(element) {
    for(key in measureControls) {
        var control = measureControls[key];
        //control.geodesic = element.checked;
    }
}


function onFeaturesSelect(feature){
	selectedFeature = feature;
	var text = '<div id="tbpopup">';
	text += '<div class="tbTitle">'+$l3+':</div>';
	text += '<ul>';
	for (var i in feature.cluster){
		var feat = feature.cluster[i];
		var lnlt = feat.geometry.getBounds().getCenterLonLat();
		ftrDesc[i] = feat.attributes;
		text += "<li><a href=\"javascript:replaceContentPopup('"+ i +"'," + lnlt.lon + "," + lnlt.lat + ")\">" + feat.attributes.name + "</a></li>";
	}
	text += '</ul></div>';
	if (text.search("<script") != -1) {
	    text = "Content contained Javascript! Escaped content below.<br />" + text.replace(/</g, "&lt;");
	}
	popupFeatures = new OpenLayers.Popup.FramedCloud("chickens",
				feature.geometry.getBounds().getCenterLonLat(),
				new OpenLayers.Size(400,350),
				text,
				null,true, onPopupClose);
			
	feature.popup = popupFeatures;
	map.addPopup(popupFeatures);
	if (feature.cluster.length == 1) {
		var feat = feature.cluster[i];
		var lnlt = feat.geometry.getBounds().getCenterLonLat();
		replaceContentPopup(i,lnlt.lon,lnlt.lat);
	}
}

function onFeaturesUnselect(feature){
	/*
	map.removePopup(feature.popup);
	feature.popup.destroy();
	feature.popup = null;  /**/
	if(feature.popup) {
	    map.removePopup(feature.popup);
	    feature.popup.destroy();
	    delete feature.popup;
	}
}

function onPopupClose(event) {
	select.unselectAll();
}

function replaceContentPopup(id,lon,lat){
	var mtb = document.getElementById("tbpopup");
	var prnt = mtb.parentNode;
	var cont = "<div class=\"tbTitle\">"+ ftrDesc[id].name + "</div><div><div style=\"text-align:center;\"><a href=\"javascript:viewProject(" + lon +", " + lat+")\">" + ftrDesc[id].description +"</a></div></div>";
	mtb.innerHTML = cont;
	popupFeatures.updateSize();
}

function viewProject(lon, lat) {
	map.setCenter(new OpenLayers.LonLat(lon, lat), 18);
	var element = document.getElementById('chickens_close');
	if(document.createEvent){
		// dispatch for firefox + others
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent('click', true, true ); // event type,bubbling,cancelable
		element.dispatchEvent(evt);
	}
	else{
		// dispatch for IE
		var evt = document.createEventObject();
		element.fireEvent('onclick',evt);
	}
	if (popupFeatures){
		map.removePopup(popupFeatures);
	}
}

function getURLParams(){
	var re = new Object();
	var strHref = window.location.href;
	if (strHref.indexOf("?") > -1){
		var strQueryString = strHref.substr(strHref.indexOf("?")+1).toLowerCase();
		var aQueryString = strQueryString.split("&");
		for (var iParam=0; iParam<aQueryString.length; iParam++){
			var aParam = aQueryString[iParam].split("=");
			eval('re.'+aParam[0]+' = "'+unescape(aParam[1])+'";');
		}
	}
	return re;
}





function overlay_getTileURL(bounds) {
	var res = this.map.getResolution();
	var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
	var y = Math.round((bounds.bottom - this.tileOrigin.lat) / (res * this.tileSize.h));
	var z = this.map.getZoom();
	if (mapBounds.intersectsBounds( bounds ) && z >= mapMinZoom && z <= mapMaxZoom ) {
		var path = z + "/" + x + "/" + y + "." + this.type;
		var url = this.url;
		if (url instanceof Array) {
			url = this.selectUrl(path, url);
		} 
		return url + path;
	} else {
		return "none.png";
	}
}


function addSlideshow(slideshow, detail) {
	var maxWidth = getWindowWidth();
	var maxHeight = getWindowHeight();
	
	var close = '<div class="closeBox"><a href="" onclick="return removeSlideshow();">Đóng lại</a></div>'
	var o = '<table border=0 cellpadding=0 cellspacing=0 width='+maxWidth+' height='+maxHeight+' class="overlayWrap">';
	o += '<tr><td align=center height='+maxHeight+' valign=top><div onclick="return removeSlideshow();" class="overlaySlide" style="height:'+maxHeight+'px; width:100%;">'+slideshow+'</div></td>';
	o += '<td align=left class="overlayDetail" valign=top width="300" height='+maxHeight+'>'+close+detail+'</td></tr>';
	o += '</table>';
	
	// setup div
	overlaySlideshow = document.createElement('div');
	overlaySlideshow.id = 'overlaySlideshow';
	overlaySlideshow.style.height = maxHeight + 'px';
	overlaySlideshow.style.width = maxWidth + 'px';
	overlaySlideshow.style.display = 'none';
	overlaySlideshow.innerHTML = '<div id="overlayContent" style="width:100%;">'+o+'</div>';
	document.getElementById('map').parentNode.appendChild(overlaySlideshow);
	
	animateDiv = document.createElement('div');
	animateDiv.style.position = 'absolute';
	animateDiv.style.zIndex = 19999;
	animateDiv.style.backgroundColor = '#555';
	document.getElementById('map').parentNode.appendChild(animateDiv);
	
	if (typeof mousePosY == 'undefined' || mousePosY == '' || mousePosY<=0) {
		mousePosY = maxHeight/2;
		mousePosX = maxWidth/2;
	}
	intervalTop = mousePosY/50;
	intervalLeft = mousePosX/50;
	intervalBottom = (maxHeight-mousePosY)/50;
	intervalRight = (maxWidth-mousePosX)/50;
	intervalOpacity = 0.5;
	setTimeout('animateSlideshow('+mousePosY+', '+mousePosX+', '+(maxHeight-mousePosY)+', '+(maxWidth-mousePosX)+', '+maxHeight+', '+maxWidth+', 1)', 30);
}

function animateSlideshow(top, left, bottom, right, maxH, maxW, opacity) {
	var h = maxH - top - bottom, w = maxW - left - right;
	if (top<=0 || left<=0) {
		overlaySlideshow.style.display = 'block';
		document.getElementById('overlaySlideshow').parentNode.removeChild(animateDiv);
		animateDiv = null;
	} else {
		animateDiv.style.top = top + 'px';
		animateDiv.style.left = left + 'px';
		animateDiv.style.width = w + 'px';
		animateDiv.style.height = h + 'px';
		animateDiv.style.opacity = opacity/10;
		animateDiv.style.filter = 'alpha(opacity='+(opacity*10)+')';
		intervalTop = intervalTop*1.5;
		intervalLeft = intervalLeft*1.5;
		intervalBottom = intervalBottom*1.5;
		intervalRight = intervalRight*1.5;
		setTimeout('animateSlideshow('+(top-intervalTop)+','+(left-intervalLeft)+','+(bottom-intervalBottom)+','+(right-intervalRight)+','+maxH+','+maxW+','+(opacity+intervalOpacity)+')', 30);
	}
}

function removeSlideshow() {
	var parentNode = document.getElementById('overlaySlideshow').parentNode;
	parentNode.removeChild(overlaySlideshow);
	overlaySlideshow = '';
	el = document.getElementById('map');
	if (document.createEventObject) {
		el.fireEvent("onclick");
	} else {
		var evt = document.createEvent("HTMLEvents");
        evt.initEvent('click', true, true ); // event type,bubbling,cancelable
        el.dispatchEvent(evt);
	}
	return false;
}

function removeClickEvent(element) {
	OpenLayers.Event.observe(element, 'mouseup',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
	OpenLayers.Event.observe(element, 'click',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
	OpenLayers.Event.observe(element, 'mousedown',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
	OpenLayers.Event.observe(element, 'dblclick',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
}

function getWindowHeight() {
	if (self.innerHeight) return self.innerHeight;
	if (document.documentElement && document.documentElement.clientHeight)
		return document.documentElement.clientHeight;
	if (document.body) return document.body.clientHeight;
		return 0;
}

function getWindowWidth() {
	if (self.innerWidth) return self.innerWidth;
	if (document.documentElement && document.documentElement.clientWidth)
		return document.documentElement.clientWidth;
	if (document.body) return document.body.clientWidth;
		return 0;
}

function resize() {
	var map = document.getElementById("map");
	map.style.height = (getWindowHeight()-20) + "px";
	map.style.width = (getWindowWidth()-20) + "px";
	if (map.updateSize) { map.updateSize(); }
}

onresize=function(){ resize(); };

from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.api import urlfetch
from google.appengine.api import mail
import urllib
from models import Project
from pylib import khongdau, json_projects

def isMobile(m):
	a = str(m).lower()
	return (a.find("ipad") >=0 or a.find("iphone") >=0 or a.find("android") >=0 or a.find("ipod") >=0 or a.find("opera mini") >=0 or a.find("blackberry") >=0 or a.find("palm") >=0 or a.find("hiptop") >=0 or a.find("avantgo") >=0 or a.find("plucker") >=0 or a.find("xiino") >=0 or a.find("blazer") >=0 or a.find("elaine") >=0 or a.find("smartphone") >=0 or a.find("opera mobi") >=0 or a.find("iemobile") >=0 or a.find("sonyericsson") >=0 or a.find("lg ") >=0 or a.find("samsung") >=0 or a.find("nokia") >=0 or a.find("symbian") >=0 or a.find("vodafone") >=0 or a.find("pocket") >=0 or a.find("o2") >=0 or a.find("mobile") >=0 or a.find("phone") >=0 or a.find("ktouch") >=0)

class MainHandler(webapp.RequestHandler):
    def get(self):
		if isMobile(self.request.headers['User-Agent']):
			self.redirect('/labs/index.html')
		else:
			self.redirect('/map/index.html')
	

class SearchHandler(webapp.RequestHandler):
	def get(self):
		search = urllib.unquote(self.request.get('q'))
		results = Project.all().search(khongdau(search)).fetch(10)
		self.response.out.write(json_projects(results))

class InsertHandler(webapp.RequestHandler):
	def get(self):
		pid = urllib.unquote(self.request.get("pid"))
		name = urllib.unquote(self.request.get("name"))
		description = urllib.unquote(self.request.get("desc"))
		type = urllib.unquote(self.request.get("type"))
		district = urllib.unquote(self.request.get("district"))
		city = urllib.unquote(self.request.get("city"))
		lat = urllib.unquote(self.request.get("lat"))
		lon = urllib.unquote(self.request.get("lon"))
		address = urllib.unquote(self.request.get("address"))
		zoom = urllib.unquote(self.request.get("zoom"))
		link = urllib.unquote(self.request.get("link"))
		investor = urllib.unquote(self.request.get("investor"))
		
		if pid is not None and name is not None and type is not None and district is not None and city is not None and zoom is not None and link is not None:
			proj = Project()
			pr = Project.gql("WHERE pid = :1", pid)
			p = pr.count(2)
			if p >= 1:
				proj = pr[0]
			
			proj.pid = pid
			proj.name = name
			proj.description = description
			proj.type = type
			proj.investor = investor
			proj.address = address
			proj.district = district
			proj.city = city
			proj.lat = lat
			proj.lon = lon
			proj.link = link
			proj.zoom = zoom
			proj.put()
			self.response.out.write("ok")
		else:
			self.response.out.write("Fail: "+khongdau(name))

class ContactHandler(webapp.RequestHandler):
	def post(self):
		user_address = self.request.get("mail")
		if mail.is_email_valid(user_address):
			receiver_address = "admin@qhoach.com"
			name = urllib.unquote(self.request.get('name'))
			subject = urllib.unquote(self.request.get('subject'))
			message = urllib.unquote(self.request.get('message'))
			body = "Sent by " + user_address
			body = body + """
---------
"""
			body = body + message + """

---------
Sent via www.qhoach.com
"""
			mail.send_mail(name+" <"+receiver_address+">", receiver_address, subject, body)
			if self.request.get('copy') == '1':
				body = """
Thank for your message via www.qhoach.com. That is message you sent:
---------

""" + message
				mail.send_mail("QHoach.com Service <"+receiver_address+">", name+" <"+user_address+">", subject, body)
			self.response.out.write("Sent successfully")
		else :
			self.response.out.write("Email invalid")

class KMLHandler(webapp.RequestHandler):
	def get(self):
		project = urllib.unquote(self.request.get("p"))
		kml = urllib.unquote(self.request.get("kml"))
		
		rpc = urlfetch.create_rpc(deadline=10)
		urlfetch.make_fetch_call(rpc, "http://vn.qhoach.com/quyhoach/demo/"+project+"/"+kml)
		try:
			obj = rpc.get_result()
			if obj.status_code == 200:
				self.response.headers.add_header("Content-Type", "application/kml")
				self.response.out.write(obj.content)
			else:
				self.response.out.write("")
		except urlfetch.DownloadError:
			self.response.out.write("")

class BCCIHandler(webapp.RequestHandler):
    def get(self):
			self.redirect('/profiles/bcci/index.html')

def main():
    application = webapp.WSGIApplication([
					('/', MainHandler), 
					('/contact', ContactHandler),
					('/search', SearchHandler),
					('/kml', KMLHandler),
					('/asfjgdguew', InsertHandler),
					('/quyhoach/demo/bcci/bcci-full.html', BCCIHandler),
					], debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()

from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.db import Key
from google.appengine.api import users
import urllib
import os
import cgi
from google.appengine.ext.webapp import template
import models
from pylib import json_news

def addNews(msg=""):
	template_values = {
		'message': msg,
	}
	path = os.path.join(os.path.dirname(__file__), 'template/addnews.html')
	return template.render(path, template_values)
      

class NewsHandler(webapp.RequestHandler):
	def get(self):
		self.response.out.write("OK");

class AddNewsHandler(webapp.RequestHandler):
#	@login_required
	def get(self):
#		if users.is_current_user_admin():
			self.response.out.write(addNews())
#		else :
#			self.error(403)
		
	def post(self):
		id = cgi.escape(self.request.get('project'))
		n = models.News()
		n.projectKey = models.Project.get_by_id(int(id))
		n.title = cgi.escape(self.request.get('title'))
		n.teaser = cgi.escape(self.request.get('teaser'))
		n.source = cgi.escape(self.request.get('source'))
		n.link = cgi.escape(self.request.get('link'))
		n.put()
		self.response.out.write(addNews("Added successfully"))

class LatestNewsHandler(webapp.RequestHandler):
	def get(self):
		l = models.News.gql("ORDER BY timestamp DESC").fetch(5);
		self.response.out.write(json_news(l))
		
def main():
    application = webapp.WSGIApplication([
					('/news', NewsHandler),
					('/news/add', AddNewsHandler),
					('/news/latest', LatestNewsHandler),
					], debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()

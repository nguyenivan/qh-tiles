var ftrDesc = [];

function init(id, kmlName, logoFile, bounds){
	QHM.constructor(id, {gsat: false});
	
	QHM.setBounds(new OpenLayers.Bounds(bounds[0], bounds[1], bounds[2], bounds[3]));
	
	var rootPath1 = 'http://tile';
	var rootPath2 = '.qhoach.com/tiles/';
	
	//var rootPath3 = 'http://ts';
	//var rootPath4 = '.qhoach.com/tilecache/HCMC/20101029/';
	//QHM.setTileMap("tiles", [rootPath1+'0'+rootPath2, rootPath1+'1'+rootPath2, rootPath1+'2'+rootPath2, rootPath1+'3'+rootPath2, rootPath3+'0'+rootPath4, rootPath3+'1'+rootPath4, rootPath3+'2'+rootPath4, rootPath3+'3'+rootPath4]);
	QHM.setTileMap("tiles", [rootPath1+'0'+rootPath2, rootPath1+'1'+rootPath2, rootPath1+'2'+rootPath2, rootPath1+'3'+rootPath2]);
	
	QHM.activate();
	
	QHM.addKML("kml/"+kmlName+".kml", {
		cluster: true, clusterDist: 50,
		onSelect: selectFeature,
		logo: logoFile
	});
	
	QHM.replaceInputs();
	
	buildHeader();
	
	QHM.addSearch(queryProjects);
	QHM.addRightPanel(rightContent);
	QHM.changeViewport();
}

function queryProjects(keyword) {
	OpenLayers.loadURL("/search", {'q':keyword}, this, function(data){
		if (data.status == 200) {
			eval('viewProjects('+data.responseText+');');
		}
	});
}

function viewProjects(data) {
	var output = '<div id="resultContent">';
	
	if (data.length<=0) {
		output += '<i>&nbsp;&nbsp;'+$l18+'</i>';
	}
	
	for(i=0; i<data.length; i++) {
		var item = data[i];
		var lonlat = new OpenLayers.LonLat(item.lon, item.lat);
		lonlat = lonlat.transform(QHM.map.displayProjection, QHM.map.projection);
		output += '<div class="project"><a href="" onclick="viewProject(\'' + lonlat.lon + '\', \'' + lonlat.lat + '\', \'' + item.zoom + '\');return false;" title="'+$l19+': '+item.investor+'">' + item.name + ' - ' + item.district + '</a></div>';
	}
	
	output += '</div>';
	
	QHM.viewResult(output);
}

function selectFeature(feature, id){
	var text = '<div id="QHp">';
	text += '<div class="QHpT">'+$l3+':</div>';
	text += '<ul style="max-height:200px; overflow:auto;">';
	for (var i in feature.cluster){
		var feat = feature.cluster[i];
		var lnlt = feat.geometry.getBounds().getCenterLonLat();
		ftrDesc[i] = feat.attributes;
		text += "<li><a href=\"javascript:replaceContentPopup('"+ i +"'," + lnlt.lon + "," + lnlt.lat + ")\">" + feat.attributes.name + "</a></li>";
	}
	text += '</ul></div>';
	if (text.search("<script") != -1) {
	    text = "Content contained Javascript! Escaped content below.<br />" + text.replace(/</g, "&lt;");
	}
	document.getElementById(id).innerHTML = text;
	
	if (feature.cluster.length == 1) {
		var feat = feature.cluster[i];
		var lnlt = feat.geometry.getBounds().getCenterLonLat();
		replaceContentPopup(i,lnlt.lon,lnlt.lat);
	}
}

function replaceContentPopup(id,lon,lat){
	var mtb = $g("QHp");
	var prnt = mtb.parentNode;
	var cont = "<div class=\"QHpT\">"+ ftrDesc[id].name + "</div><div><div style=\"text-align:center;\"><a href=\"javascript:viewProject(" + lon +", " + lat+", 16)\">" + ftrDesc[id].description +"</a></div></div>";
	mtb.innerHTML = cont;
	QHM.popup.updateSize();
}

function viewProject(lon, lat, zoom) {
	lon = Number(lon);
	lat = Number(lat);
	zoom = Number(zoom);
	QHM.map.zoomTo(zoom);
	QHM.map.panTo(new OpenLayers.LonLat(lon, lat));
}

function buildHeader() {
	var newDiv = $c('div');
	newDiv.style.width = "100%";
	newDiv.style.position = "absolute";
	newDiv.style.top = "0px";
	newDiv.style.left = "0px";
	newDiv.style.zIndex = 900;
	newDiv.innerHTML = "<table width='100%' border=0 cellpadding=0 cellspacing=0><tr><td id='QHHead-fix'><div id='QHHead'>&nbsp;</div></td><td width='261' id='QHHead-fix2'>&nbsp;</td></tr></table>";
	$g(QHM.id).childNodes[0].appendChild(newDiv);
	
	removeClickEvent($g('QHHead'));
	
	var newDiv2 = $c('div');
	newDiv2.style.position = "absolute";
	newDiv2.style.top = "1px";
	newDiv2.style.left = "0px";
	newDiv2.style.zIndex = 901;
	newDiv2.innerHTML = '<a href="/corp/index.html"><img border=0 src="img/qhoachlogo.png" /></a>';
	$g(QHM.id).childNodes[0].appendChild(newDiv2);
}

function rightContent(id) {
	$g(id).innerHTML = "Loading...";
	OpenLayers.loadURL("/news/latest", {}, this, function(data){
		if (data.status == 200) {
			eval('viewLatestNews('+data.responseText+', "'+id+'");');
		}
	});
}

function viewLatestNews(data, id) {
	out = ""
	if (data.length>0) {
		a = data[0];
		out += '<div class="news_head"><a href="'+a.project.link+'">'+a.title+'</a></div>';
		out += '<div>'+a.teaser+'</div>';
		out += '<div class="news_src"><span>'+$l20+':</span> <a href="'+a.link+'">'+a.source+'</a></div>';
		for(i=1; i<data.length; i++) {
			a = data[i];
			out += '<div><a>'+a.title+'</a></div>';
		}
	}
	
	$g(id).innerHTML = out;
}

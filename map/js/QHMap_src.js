function $g(id) {
	return document.getElementById(id);
}

function $c(tagName) {
	return document.createElement(tagName);
}

function $n(variable) {
	return (typeof variable != 'undefined' && variable != '');
}

OpenLayers.Util.getImagesLocation = function(){
	return 'img/';
};

OpenLayers.Control.PanZoomBar.prototype.draw = function(px) {
	OpenLayers.Control.prototype.draw.apply(this, arguments);
	px = this.position.clone();
	
	this.buttons = [];

	var sz = new OpenLayers.Size(20,23);
	var centered = new OpenLayers.Pixel(0, 0);

	px.y = centered.y+5;
	this._addButton("zoomin", "zoomin.png", px.add(1, 0), sz);
	centered = this._addZoomBar(px.add(1, sz.h));
	this.slider.style.width = '22px';
	this.slider.style.height = '13px';
	this.slider.childNodes[0].style.width = '22px';
	this.slider.childNodes[0].style.height = '13px';
	this._addButton("zoomout", "zoomout.png", centered, sz);
	return this.div;
};

OpenLayers.Control.LayerSwitcher.prototype.loadContents = function() {
	var theHTML = '';
	theHTML += '<a id="control-none" onclick="toggleControl(this, \'none\');" class="active">'+$l8+'</a>';
	theHTML += '<a id="control-line" onclick="toggleControl(this, \'line\');">'+$l9+'</a>';
	theHTML += '<a id="control-polygon" onclick="toggleControl(this, \'polygon\');">'+$l10+'</a>';
	
	var DistanceDiv = $c("div");
	DistanceDiv.id = 'DistanceDiv';
	DistanceDiv.innerHTML = theHTML;
	
	
	this.layersDiv = $c("div");
	this.layersDiv.id = this.id + "_layersDiv";
	OpenLayers.Element.addClass(this.layersDiv, "layersDiv");
	
	this.baseLbl = $c("div");
	this.dataLbl = $c("div");
	this.dataLayersDiv = $c("div");
	
	this.baseLayersDiv = $c("div");
	OpenLayers.Element.addClass(this.baseLayersDiv, "baseLayersDiv");
	
	this.layersDiv.appendChild(this.baseLayersDiv);
	
	this.div.appendChild(this.layersDiv);
	
	var imgLocation = OpenLayers.Util.getImagesLocation();
	var sz = new OpenLayers.Size(0,0);
	
	var img = imgLocation + 'layer-switcher-maximize.png';
	this.maximizeDiv = OpenLayers.Util.createAlphaImageDiv(
				"OpenLayers_Control_MaximizeDiv",
				null, sz, img, "absolute"
	);
	var img = imgLocation + 'layer-switcher-minimize.png';
	var sz = new OpenLayers.Size(0,0);      
	this.minimizeDiv = OpenLayers.Util.createAlphaImageDiv(
				"OpenLayers_Control_MinimizeDiv",
				null, sz, img, "absolute"
	);
	
	this.div.appendChild(DistanceDiv);
	removeClickEvent(this.div);
};

OpenLayers.Control.LayerSwitcher.prototype.onInputClick = function(e) {
	if (!this.inputElem.disabled) {
		if (this.inputElem.type == "radio") {
			this.layer.map.setBaseLayer(this.layer);
			if (!this.inputElem.checked) {
				this.inputElem.checked = true;
				QHM.replaceInputs();
			}
		} else {
			this.inputElem.checked = !this.inputElem.checked;
			this.layerSwitcher.updateMap();
		}
	}
	OpenLayers.Event.stop(e);
};

OpenLayers.Map.prototype.zoomToMaxExtent = function(options) {
	this.zoomToExtent(QHM.defaultBounds);
};

/**
 *  options = {
 *  		gmap: true,
 *  		gsat: true,
 *  		ghyb: true,
 *  		maxZoom: 20,
 *  		minZoom: 14,
 *  }
 */

var currHash = "";

QHM = function() {
	this.constructor = function(id, options) {
		this.id = id; this.options = options;
		this.checkmouseposition = false;
		this.mousePosX = '';
		this.mousePosY = '';
		this.outputCoord = '';
		this.popup = null;
		
		this.setOpt();
		
		var opt = {
			controls: [],
			projection: new OpenLayers.Projection("EPSG:900913"),
			displayProjection: new OpenLayers.Projection("EPSG:4326"),
			units: "m",
			maxResolution: 156543.0339,
			maxExtent: new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508.34)
		};
		this.map = new OpenLayers.Map('map', opt);
		
		if (this.options.gmap) {
			this.map.addLayer(new OpenLayers.Layer.Google($l1, 
			{type: google.maps.MapTypeId.ROADMAP, sphericalMercator: true, numZoomLevels: this.options.maxZoom+1, transitionEffect: 'resize'}));
		}
		if (this.options.gsat) {
			this.map.addLayer(new OpenLayers.Layer.Google($l2, 
			{type: google.maps.MapTypeId.SATELLITE, sphericalMercator: true, numZoomLevels: this.options.maxZoom+1, transitionEffect: 'resize'}));
		}
		if (this.options.ghyb) {
			this.map.addLayer(new OpenLayers.Layer.Google($l2, 
			{type: google.maps.MapTypeId.HYBRID, sphericalMercator: true, numZoomLevels: this.options.maxZoom+1, transitionEffect: 'resize'}));
		}
		this.addL();
	};
	
	this.replaceInputs = function() {
		var inputs = document.getElementsByTagName('input');
		var images = Array();
		for (i=0; i<inputs.length; i++) {
			if (inputs[i].type == "radio") {
				if (inputs[i].previousSibling != null && $n(inputs[i].previousSibling.src)) {
					return;
				}
				images[i] = $c("img");
				images[i].className = "radio";
				images[i].onmouseup = this.pushedInput;
				
				if(inputs[i].checked) {
					images[i].src = "img/radioon.png";
				} else {
					images[i].src = "img/radiooff.png";
				}
				
				inputs[i].parentNode.insertBefore(images[i], inputs[i]);
				inputs[i].style.visibility = "hidden";
				inputs[i].style.marginLeft = "-10px";
			}
		}
	};
	
	this.pushedInput = function() {
		var element = this.nextSibling;
		if (element.type == "radio" && !element.checked) {
			var images = element.parentNode.getElementsByTagName('img');
			for (i=0; i<images.length; i++) {
				images[i].src = "img/radiooff.png";
			}
			fireClick(element);
			this.src = "img/radioon.png";
		}
	};
	
	this.addSearch = function(resultCallback) {
		var newDiv = $c('div');
		newDiv.id = 'searchBox';
		$g(this.id).parentNode.appendChild(newDiv);
		
		newDiv.innerHTML = '<input type="text" id="searchInput" onkeyup="return QHM.typeIntoSearch(event);" />&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="return QHM.enterKeyword();" id="searchBtn" value="'+$l16+'" />';
		
		this.viewHint();
		this.activateSearch();
		
		this.sCall = resultCallback;
		this.sCont = '';
	};
	
	this.viewResult = function(content) {
		var el = $g('searchContent');
		el.innerHTML = content;
		el.style.display = 'block';
		$g('searchLoading').style.visibility = 'hidden';
		$g('searchHint').style.visibility = 'visible';
	};
	
	this.expandResult = function() {
		var el = $g('searchContent');
		if (el.style.display == 'none') {
			el.style.display = 'block';
		} else {
			el.style.display = 'none';
		}
	}
	
	this.activateSearch = function(){
		var element = $g('searchInput');
		if (element.value == '') {
			element.className = 'show';
		} else {
			element.className = '';
		}
	};
	
	this.typeIntoSearch = function(e) {
		var keynum = null;
		if(window.event) {
			keynum = e.keyCode
		} else if(e.which) {
			keynum = e.which
		}
		
		if (keynum == 13) { // press ENTER
			this.enterKeyword();
		} else {
			this.activateSearch();
		}
		return false;
	};
	
	this.enterKeyword = function() {
		var element = $g('searchInput');
		this.sCall.call(this, element.value);
		$g('searchLoading').style.visibility = 'visible';
		$g('searchHint').style.visibility = 'hidden';
		$g('searchContent').style.display = 'none';
	};
	
	this.viewHint = function() {
		var output = '<div id="searchContent"></div>';
		output += '<div id="searchHint">'+$l15+':<span id="someHint"></span>...</div>';
		output += '<div id="searchLoading"><img src="img/loading.gif" /> '+$l17+'</div>';
		output += '<div id="searchHide"><span class="collapse" onclick="QHM.expandResult();"><img src="img/minimize.png" /></span></div>';
		
		var newDiv = $c('div');
		newDiv.id = 'searchResult';
		newDiv.innerHTML = output;
		$g(this.id).parentNode.appendChild(newDiv);
		this.sCont = newDiv;
		
		$g('searchLoading').style.visibility = 'hidden';
		$g('searchContent').style.display = 'none';
		
		var hint = '';
		for (i=0; i<$key.length; i++) {
			hint += ' <a href="" onclick="QHM.fillHint(\''+$key[i]+'\');return false;">'+$key[i]+'</a>,';
		}
		$g("someHint").innerHTML = hint;
	};
	
	this.fillHint = function(val) {
		$g("searchInput").value = val;
		this.activateSearch();
		this.enterKeyword();
	};
	
	this.addL = function() {
		var newDiv = $c('div');
		newDiv.style.position = 'absolute';
		newDiv.style.bottom = '9px';
		newDiv.style.left = '80px';
		newDiv.style.zIndex = 9999;
		newDiv.innerHTML = '<a href="/corp/index.html"><img src="/map/QHoach.com.png" border=0 /></a>';
		$g(this.id).childNodes[0].appendChild(newDiv);
		
		var newDiv = $c('div');
		newDiv.style.position = 'absolute';
		newDiv.style.bottom = '1px';
		newDiv.style.left = '190px';
		newDiv.style.zIndex = 9999;
		newDiv.innerHTML = '<iframe src="link.html" width=290 height=23 frameborder=0 scrolling="no"></iframe>';
		$g(this.id).childNodes[0].appendChild(newDiv);
	}
	
	this.addRightPanel = function(contentCallback) {
		var QHd = $c('div');
		QHd.id = 'QHd';
		QHd.setAttribute('unselectable', 'on');
		$g(this.id).parentNode.appendChild(QHd);
		
		var QHdX = $c('div');
		QHdX.id = 'QHdX';
		QHdX.innerHTML = '<a onclick="QHE.toggleR();return false;" title="'+$l21+'"><span>&nbsp;</span></a>';
		QHd.appendChild(QHdX);
		
		var QHdC = $c('div');
		QHdC.id = 'QHdC';
		QHd.appendChild(QHdC);
		contentCallback.call(this, 'QHdC');
		
		QHd.style.height = (this.getWindowHeight()-200) + 'px';
	};
	
	this.setMsg = function(message) {
		var content = '<table border=0 cellpadding=0 cellspacing=0 width="100%" height="40"><tr>';
		content += '<td width="45%">&nbsp;</td>';
		content += '<td align="center"><div class="topMsgContent">'+message+'</div></td>';
		content += '<td width="45%">&nbsp;</td>';
		content += '</tr></table>';
		
		var newDiv = $c('div');
		newDiv.id = 'topMsg';
		newDiv.style.position = 'absolute';
		newDiv.style.width = '100%';
		newDiv.style.bottom = '0px';
		newDiv.style.left = '0px';
        newDiv.style.height = "40px";
		newDiv.style.zIndex = 990;
		newDiv.innerHTML = content;
		$g(this.id).childNodes[0].appendChild(newDiv);
		this.topMsg = newDiv;
        QHE.slideMsg(-40);
	};
    
	this.removeMsg = function() {
		if ($n(this.topMsg)) {
			$g(this.id).childNodes[0].removeChild(this.topMsg);
			this.topMsg = '';
		}
	}
	
	this.addKML = function(urlKML, options){
		// options: cluster (BOOLEAN), clusterDist (INTEGER), onRead (callback function when a feature is read), onSelect (callback function when selecting a feature)
		var strategy = [new OpenLayers.Strategy.Fixed()];
		if (options.cluster) {
			var dist = (options.clusterDist)?options.clusterDist:50
			strategy.push(new OpenLayers.Strategy.Cluster({distance:dist}));
		}
		
		var kmlstyle = new OpenLayers.Style({
			graphicTitle: "${name}",
			externalGraphic: "${icon}",
			graphicWidth: 80,
			graphicHeight: 80,
			graphicXOffset: -45,
			graphicYOffset: -40, 
			graphicOpacity: 1,
			pointerEvents: "visiblePainted",
			pointRadius: 20,
			cursor: "pointer",
			label : ""
		}, {
			context: {
				name: function(feature){
					if (feature.cluster){
						last = feature.cluster.length - 1;
						if (last < 1) {
							feature.attributes.name = feature.cluster[last].attributes.name;
							feature.attributes.icon = feature.cluster[last].style.externalGraphic;
							if ($n(options.onRead)) {
								options.onRead.call(this, feature.cluster[last].attributes.type, feature.cluster[last].attributes.name, feature.cluster[last].geometry);
							}
						}
						else {
							n = '';
							for (var i=0; i<feature.cluster.length;i++) {
								n += feature.cluster[i].attributes.name;
								if (i<last) n+=', ';
								if ($n(options.onRead)) {
									options.onRead.call(this, feature.cluster[i].attributes.type, feature.cluster[i].attributes.name, feature.cluster[i].geometry);
								}
							}
							feature.attributes.icon = "images/logo/"+options.logo;
							feature.attributes.name = n;
						}
					}
					return feature.attributes.name;
				},
				icon: function(feature){return feature.attributes.icon;},
				size: function(feature){return feature.attributes.size;},
				offset: function(feature) {return feature.attributes.offset;},
				opacity: function(feature){return feature.cluster ? 1: feature.attributes.opacity;},
				code: function(feature) {return feature.attributes.code;}
			}
		});
		
		this.kml = new OpenLayers.Layer.Vector("KML", {
			projection: this.map.displayProjection,
			strategies: strategy,
			protocol: new OpenLayers.Protocol.HTTP({
				url: urlKML,
				format: new OpenLayers.Format.KML({
					extractStyles: true,
					extractAttributes: true
				})
			}),
			styleMap: kmlstyle,
			resolutions: [183361.3617, 0.699158175],
			displayInLayerSwitcher: false
		});
		
		this.map.addLayer(this.kml);

		this.selCtrl = new OpenLayers.Control.SelectFeature(this.kml, {
			onSelect: QH_select,
			onUnselect: QH_unselect,
            toggle: true, clickout:false
		});
        this.selCtrl.handlers['feature'].stopDown = false;
        this.selCtrl.handlers['feature'].stopUp = false;
		this.map.addControl(this.selCtrl);
		this.selCtrl.activate();
		
		this.selCall = options.onSelect;
	};
	
	this.activate = function() {
		this.defaultBounds.transform(this.map.displayProjection, this.map.projection);
		this.mapBounds.transform(this.map.displayProjection, this.map.projection);
		
		this.map.zoomToMaxExtent();
		
		this.map.addControl(new OpenLayers.Control.PanZoomBar({
			zoomStopWidth: this.zsWidth,
			zoomStopHeight: this.zsHeight
		}));
		
		this.map.addControl(new OpenLayers.Control.MouseDefaults());
		//this.map.addControl(new OpenLayers.Control.MousePosition());
		
		var switcherControl = new OpenLayers.Control.LayerSwitcher({'roundedCorner': false});
		this.map.addControl(switcherControl);
		switcherControl.maximizeControl();
		
		this.addDistanceControl();
        
        this.map.events.register("zoomend", this.kml, function(e){
            QH_unselect(e);
        });
        this.map.events.register("moveend", this.map, function(e){
			var center = QHM.map.getCenter().transform(QHM.map.projection, QHM.map.displayProjection);
			var newHash = "#"+center.lon+"_"+center.lat+"_"+QHM.map.getZoom();
			if (currHash != newHash) {
				currHash = newHash;
				window.location.hash = newHash;
			}
		});
	};
	
	this.setBounds = function(bounds) {
		this.mapBounds = bounds;
	};
	
	this.setZoomBar = function(width, height) {
		this.zsWidth = width;
		this.zsHeight = height;
	};
	
	this.setTileMap = function(name, tileURL) {
		this.map.addLayer(new OpenLayers.Layer.TMS(name, tileURL,
					{
						type: 'png', getURL: QH_getTileURL, alpha: true, 
						isBaseLayer: false, displayInLayerSwitcher: false,
						transitionEffect: 'resize', zoomOffset: 3
					}
		));
	};
	
	this.setOpt = function() {
		this.options.gmap = (typeof this.options.gmap == 'undefined')?true:this.options.gmap;
		this.options.gsat = (typeof this.options.gsat == 'undefined')?true:this.options.gsat;
		this.options.ghyb = (typeof this.options.ghyb == 'undefined')?true:this.options.ghyb;
		
		this.options.maxZoom = $n(this.options.maxZoom)?this.options.maxZoom:20;
		this.options.minZoom = $n(this.options.minZoom)?this.options.minZoom:14;
		
		this.defaultBounds = new OpenLayers.Bounds(106.63656, 10.71145, 106.69236, 10.73413);
		this.mapBounds = $n(this.mapBounds)?this.mapBounds:this.defaultBounds;
		
		this.zsWidth = $n(this.zsWidth)?this.zsWidth:22;
		this.zsHeight = $n(this.zsHeight)?this.zsHeight:11;
	};
	
	this.resize = function() {
		var div = $g(this.id);
		div.style.height = (this.getWindowHeight()) + "px";
		div.style.width = (this.getWindowWidth()) + "px";
		this.map.updateSize();
	};
	
	this.getWindowHeight = function() {
		if (self.innerHeight) {
			return self.innerHeight;
		}
		if (document.documentElement && document.documentElement.clientHeight) {
			return document.documentElement.clientHeight;
		}
		if (document.body) {
			return document.body.clientHeight;
		}
		return 0;
	};
	
	this.getWindowWidth = function() {
		if (self.innerWidth) {
			return self.innerWidth;
		}
		if (document.documentElement && document.documentElement.clientWidth) {
			return document.documentElement.clientWidth;
		}
		if (document.body) {
			return document.body.clientWidth;
		}
		return 0;
	};
	
	this.addDistanceControl = function() {
		var sketchSymbolizers = {
			"Point": {
				pointRadius: 4,
				graphicName: "circle",
				fillColor: "red",
				fillOpacity: 1,
				strokeWidth: 1,
				strokeOpacity: 1,
				strokeColor: "#333333"
			},
			"Line": {
				strokeWidth: 3,
				strokeOpacity: 1,
				strokeColor: "#DE9619",
				strokeDashstyle: "solid"
			},
			"Polygon": {
				strokeWidth: 2,
				strokeOpacity: 1,
				strokeColor: "#DE9619",
				fillColor: "white",
				fillOpacity: 0.3
			}
		};
		
		var stylen = new OpenLayers.Style();
		stylen.addRules([
			new OpenLayers.Rule({symbolizer: sketchSymbolizers})
		]);
		var styleMapn = new OpenLayers.StyleMap({"default": stylen});
		
		this.measureControls = {
			line: new OpenLayers.Control.Measure(
				OpenLayers.Handler.Path, {
					persist: true,
					handlerOptions: {
						layerOptions: {styleMap: styleMapn}
					}
				}
			),
			polygon: new OpenLayers.Control.Measure(
				OpenLayers.Handler.Polygon, {
					persist: true,
					handlerOptions: {
						layerOptions: {styleMap: styleMapn}
					}
				}
			)
		};
		
		var control;
		for(var key in this.measureControls) {
			control = measureControls[key];
			control.events.on({
				"measure": this.handleMeasurements,
				"measurepartial": this.handleMeasurements
			});
			this.map.addControl(control);
		}
	};
	
	this.handleMeasurements = function(event) {
		var geometry = event.geometry;
		var units = event.units;
		var order = event.order;
		var measure = event.measure;
		var out = "";
		if(order == 1) {
			out += $l11 + ": " + measure.toFixed(3) + " " + units;
		} else {
			out += $l12 + ": " + measure.toFixed(3) + " " + units + "<sup>2</" + "sup>";
		}
		QHM.outputCoord.innerHTML = out;
		QHM.outputCoord.style.display = 'block';
	};
	
	this.toggleControl = function(element, name) {
		$g('control-none').className = '';
		this.checkmouseposition = false;
		this.removeMsg();
		
		if ($n(this.outputCoord)) {
			$g('map').parentNode.removeChild(this.outputCoord);
			this.outputCoord = '';
		}
		
		for(var key in measureControls) {
			$g('control-'+key).className = '';
			var control = measureControls[key];
			if(name == key ) {
				control.activate();
				this.checkmouseposition = true;
				this.setMsg($l14);
			} else {
				control.deactivate();
			}
		}
		element.className = 'active';
		this.replaceInputs();
	};
	
	this.captureMousePosition = function(event) {
		if (document.all) {
			QHM.mPX = window.event.clientX;
			QHM.mPY = window.event.clientY;
		} else {
			QHM.mPX = event.pageX;
			QHM.mPY = event.pageY;
		}
		if (QHM.checkmouseposition) {
			if (!$n(QHM.outputCoord)) {
				QHM.outputCoord = $c('div');
				QHM.outputCoord.id = 'outputCoord';
				$g(QHM.id).parentNode.appendChild(QHM.outputCoord);
				QHM.outputCoord.style.display = 'none';
			}
			l = QHM.mPX + 10;
			t = QHM.mPY + 20;
			QHM.outputCoord.style.left = l + 'px';
			QHM.outputCoord.style.top = t + 'px';
		}
	};
	
	this.addSlide = function(slideshow, detail) {
		var maxWidth = this.getWindowWidth();
		var maxHeight = this.getWindowHeight();
		
		var close = '<div class="clxbx"><a href="" onclick="return QHM.removeSlide();">'+$l13+'</a></div>';
		var o = '<table border=0 cellpadding=0 cellspacing=0 width='+maxWidth+' height='+maxHeight+' class="ovlW">';
		o += '<tr><td align=center height='+maxHeight+' valign=top><div onclick="return QHM.removeSlide();" class="ovlShow" style="height:'+maxHeight+'px; width:100%;">'+slideshow+'</div></td>';
		o += '<td align=left class="ovlDt" valign=top width="300" height='+maxHeight+'>'+close+detail+'</td></tr>';
		o += '</table>';
		
		// setup div
		this.ovlSld = $c('div');
		this.ovlSld.id = 'ovlSld';
		this.ovlSld.style.height = maxHeight + 'px';
		this.ovlSld.style.width = maxWidth + 'px';
		this.ovlSld.style.display = 'none';
		this.ovlSld.innerHTML = '<div id="ovlCont" style="width:100%;">'+o+'</div>';
		$g('map').parentNode.appendChild(this.ovlSld);
		
		this.aniD = $c('div');
		this.aniD.style.position = 'absolute';
		this.aniD.style.zIndex = 19999;
		this.aniD.style.backgroundColor = '#555';
		$g('map').parentNode.appendChild(this.aniD);
		
		if (!$n(this.mPY) || this.mPY<=0) {
			this.mPY = maxHeight/2;
			this.mPX = maxWidth/2;
		}
		QHE.setupFS(this.mPY/50, this.mPX/50, (maxHeight-this.mPY)/50, (maxWidth-this.mPX)/50, 0.5);
		setTimeout('QHE.popupFS('+this.mPY+', '+this.mPX+', '+(maxHeight-this.mPY)+', '+(maxWidth-this.mPX)+', '+maxHeight+', '+maxWidth+', 1)', 30);
	};
	
	this.removeSlide = function() {
		if ($n(this.ovlSlds)) {
			this.ovlSld.parentNode.removeChild(this.ovlSld);
			this.ovlSld = '';
		}
		return false;
	};
	
	this.changeViewport = function() {
		var value = window.location.hash.slice(1);
		if ($n(value)) {
			var URLParam = value.split("_");
			
			if (URLParam.length > 0){
				var mlon = Number(URLParam[0]);
				var mlat = Number(URLParam[1]);
				var mzoom = Number(URLParam[2]);
				if (typeof mlon != 'undefined' && typeof mlat != 'undefined' && typeof mzoom != 'undefined') {
					if (mlon != '' && mlat != '' && mzoom != ''){
						var mlonlat = new OpenLayers.LonLat(mlon,mlat);
						mlonlat = mlonlat.transform(this.map.displayProjection, this.map.projection);
						this.viewProject(mlonlat.lon,mlonlat.lat,mzoom);
					}
				}
			}
		} else {
			this.map.zoomToMaxExtent();
		}
	};
	
	this.onkey = function(keycode) {
		if (keycode == 27) { // ESC
			if (this.ovlSld!='') {
				this.removeSlide();
			}
			this.toggleControl($g('control-none'), 'none');
		}
	};
	
	return this;
}();

window.onresize = function(){
	QHM.resize();
};

function QH_getTileURL(bounds) {
	var res = this.map.getResolution();
	var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
	var y = Math.round((bounds.bottom - this.tileOrigin.lat) / (res * this.tileSize.h));
	var z = this.map.getZoom();
	if (QHM.mapBounds.intersectsBounds(bounds) && z >= QHM.options.minZoom && z <= QHM.options.maxZoom) {
		var path = z + "_" + x + "_" + y;
		var url = this.url;
		if (url instanceof Array) {
			url = this.selectUrl(path, url);
		}
		return url + path;
	} else {
		return "img/none.png";
	}
}

function QH_select(feature) {
	QHM.popup = new OpenLayers.Popup.FramedCloud("chickens",
			feature.geometry.getBounds().getCenterLonLat(),
			new OpenLayers.Size(400,350),
			"<div id='QH_Select_ID'></div>",
			null,true, QH_popupClose
	);
	//QHM.popup = feature.popup;
	QHM.map.addPopup(QHM.popup);
	QHM.selCall.call(this, feature, 'QH_Select_ID');
	QHM.popup.updateSize();
}

function QH_unselect(feature){
	if(QHM.popup) {
	    QHM.map.removePopup(QHM.popup);
	    QHM.popup.destroy();
	    QHM.popup = null;
	}
}

function QH_popupClose(event) {
	QHM.selCtrl.unselectAll();
}

function removeClickEvent(element) {
	OpenLayers.Event.observe(element, 'mouseup',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
	OpenLayers.Event.observe(element, 'click',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
	OpenLayers.Event.observe(element, 'mousedown',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
	OpenLayers.Event.observe(element, 'dblclick',function(evt){
		OpenLayers.Event.stop(evt);
		return true;
	});
}

function fireClick(element) {
	if(document.createEvent){
		// dispatch for firefox + others
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent('mouseup', true, true ); // event type,bubbling,cancelable
		element.dispatchEvent(evt);
	}
	else{
		// dispatch for IE
		var evt = document.createEventObject();
		element.fireEvent('onmouseup',evt);
	}
}

if (document.all) {
	document.onmousemove=QHM.captureMousePosition;
} else {
	window.onmousemove=QHM.captureMousePosition;
}

window.onkeyup = function(e) {
	var keynum = null;
	if(window.event) {
		keynum = e.keyCode
	} else if(e.which) {
		keynum = e.which
	}
	QHM.onkey(keynum);
};

window.onhashchange = function(e) {
	if (currHash != window.location.hash) {
		QHM.changeViewport();
	}
};

